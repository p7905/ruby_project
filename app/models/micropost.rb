class Micropost < ActiveRecord::Base
    belongs_to :user # принадлежность пользователю
    validates :content, length: { maximum: 140 }, # ограничение длины
    presence: true # проверка что не пусто
end
