require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  test "invalid signup information" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, user: {name: "", 
                              email: "",
                              password: "", 
                              password_confirmation: ""}
    end
    assert_template 'users/new'

    assert_select 'div#<CSS id for error explanation>'
    assert_select 'div.<CSS class for field with error>'
  end

  test "valid signup information" do
    get signup_path
    assert_difference 'User.count', 1 do
      post_via_redirect users_path, user: {name: "Test", 
                              email: "Test@test.com",
                              password: "qazwsxedcrfv", 
                              password_confirmation: "qazwsxedcrfv"
                              }                       
    end
    assert_template 'users/show'
    assert_not flash.empty?
    assert is_logged_in?
  end 
end
